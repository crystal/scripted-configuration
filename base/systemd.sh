#!/usr/bin/env bash

set -x

function systemd_timer () {
	DESCRIPTION="${1}"
	NAME="${DESCRIPTION// /-}"
	INTERVAL="${2}"
	EXEC="${3}"
	USER="${4:-root}"

	cat <<EOF > "/etc/systemd/system/${NAME,,}.service"
[Unit]
Description="${DESCRIPTION}"

[Service]
Type=oneshot
ExecStart="${EXEC}"
User=${USER}
Group=systemd-journal
EOF
	cat <<EOF > "/etc/systemd/system/${NAME,,}.timer"
[Unit]
Description="${DESCRIPTION}"

[Timer]
OnCalendar=${INTERVAL}
Persistent=true

[Install]
WantedBy=timers.target
EOF
	systemctl enable "${NAME,,}.timer"
	systemctl start "${NAME,,}.timer"
}

#!/usr/bin/env bash

set -ex

: ${WPBASEDIR:=/opt/woodpecker}

source "base/secrets.sh"
source "base/users.sh"
source "base/base.sh"
source "base/docker.sh"
source "base/woodpecker.sh"

: ${RAMDISK_SIZE:=32G}
: ${GOPROXY:=0.0.0.0:9090}
: ${GO_VERSION:=1.19.4}

function apt_install_forgejo() {
    apt_install gettext-base wget
}

function setup_docker() {
    mount_ramdisk
    install_docker
}

function mount_ramdisk() {
    local dir=/var/lib/docker

    if grep -q $dir /etc/fstab ; then
	return
    fi

    mkdir -p $dir
    echo "tmpfs           $dir            tmpfs   rw,mode=0750,size=${RAMDISK_SIZE}" >> /etc/fstab
    mount $dir
}

function setup_goproxy() {
    if systemctl status goproxy ; then
	return
    fi

    # install go
    wget -c https://go.dev/dl/go$GO_VERSION.linux-amd64.tar.gz
    rm -fr /usr/local/go ; tar -C /usr/local -xzf go$GO_VERSION.linux-amd64.tar.gz

    echo 'export PATH=$PATH:/usr/local/go/bin' >> /etc/profile

    useradd --system --uid 804 -d "/opt/goproxy" goproxy
    mkdir -p /opt/goproxy/goproxy-cache
    chown -R goproxy:goproxy /opt/goproxy

    su - goproxy -c "/usr/local/go/bin/go install github.com/goproxy/goproxy/cmd/goproxy@latest"

    GOPROXY=$GOPROXY envsubst '$GOPROXY' > /etc/systemd/system/goproxy.service <<'EOF'
[Unit]
Description=goproxy

# check that our binary is executable
AssertFileIsExecutable=/opt/goproxy/go/bin/goproxy

[Service]
Type=simple
ExecStart=/opt/goproxy/go/bin/goproxy -address='${GOPROXY}' -cacher-dir='/opt/goproxy/goproxy-cache' -cacher-max-cache-bytes='8589934592'  -go-bin-name='/usr/local/go/bin/go'
Restart=always
RestartSec=10
ExecReload=/bin/kill -HUP $MAINPID
Restart=always
User=goproxy
Group=goproxy

[Install]
WantedBy=multi-user.target
EOF

    systemctl daemon-reload
    systemctl enable --now goproxy
}

function main() {
    setup_sshd

    user_setup "dachary" "sudo"
    user_setup "gapodo" "sudo"

    apt_install_forgejo
    setup_docker
    setup_woodpecker
    setup_goproxy
}

${@:-main}



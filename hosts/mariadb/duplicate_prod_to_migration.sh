#!/usr/bin/env bash
set -ex

source "../../base/secrets.sh"

# this is copy and pasted from inside.sh :/
USER_AND_DB_NAME="forgejo_migration"
ALLOWED_REMOTE_HOST="forgejo-migration.lxc.local"

SECRET_VAR="MARIADB_${USER_AND_DB_NAME^^}"
secret_get "${SECRET_VAR}"
USER_PASSWORD="${!SECRET_VAR}"

TABLES=$(mysql ${USER_AND_DB_NAME} -e 'show tables' | awk '{ print $1}' | grep -v '^Tables' )
for t in $TABLES
do
	echo "Deleting $t table from ${USER_AND_DB_NAME}"
	mysql ${USER_AND_DB_NAME} -e "DROP TABLE \`${t}\`;" 
done

mysql -e "DROP DATABASE \`${USER_AND_DB_NAME}\`;"
#mysql -e "CREATE DATABASE \`${USER_AND_DB_NAME}\`;"
#mysql -e "GRANT ALL PRIVILEGES ON ${USER_AND_DB_NAME}.* TO '${USER_AND_DB_NAME}'@'${ALLOWED_REMOTE_HOST}' IDENTIFIED BY '${USER_PASSWORD}';"


#mysqldump gitea_production | mysql ${USER_AND_DB_NAME}

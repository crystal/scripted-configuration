#!/usr/bin/env bash

L_MOUNTPOINT="/mnt/btrfs/forgejo-next"
set -ex

if [ ! -d "${L_MOUNTPOINT}" ]; then
	mkdir "${L_MOUNTPOINT}"
fi

MEM_HIGH=4
CPU_MAX=4
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = ${L_MOUNTPOINT} mnt/data none defaults,bind,create=dir 0 0
EOF
)
